#define _GNU_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>
#include <sys/mman.h>


static void test_successful_allocation() {
    void* heap = heap_init(0);
    puts("Test 1. Heap before:");
    debug_heap(stdout, heap);
    void* mem = _malloc(0);
    assert(mem != NULL);
    _free(mem);
    puts("Test 1. Heap after:");
    debug_heap(stdout, heap);
    heap_term();
}

static void test_free_one() {
    void* heap = heap_init(0);
    puts("Test 2. Heap before:");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    _free(mem1);
    puts("Test 2. Heap after:");
    debug_heap(stdout, heap);
    assert(mem2 != NULL);
    _free(mem2);
    heap_term();
}

static void test_free_two() {
    void* heap = heap_init(0);
    puts("Test 3. Heap before:");
    debug_heap(stdout, heap);

    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    void* mem3 = _malloc(256);
    void* mem4 = _malloc(512);
    _free(mem1);
    _free(mem3);
    assert(mem2 != NULL);
    assert(mem4 != NULL);
    puts("Test 3. Heap after:");
    debug_heap(stdout, heap);
    _free(mem2);
    _free(mem4);
    heap_term();
}

static void test_same_region_extension() {
    struct region* heap = heap_init(0);
    puts("Tes 4. Heap before:");
    debug_heap(stdout, heap);
    size_t mem1_size = 4096;
    void* mem1 = _malloc(mem1_size);
    void* mem2 = _malloc(REGION_MIN_SIZE * 2);
    assert(mem2 - mem1_size - offsetof(struct block_header, contents) == mem1);
    puts("Test 5. Heap after:");
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    heap_term();
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

void test_different_region_extension() {
    heap_init(0);
    void* mmap_addr = map_pages(HEAP_START, 10, MAP_FIXED);
    assert(mmap_addr);
    puts("Test 6. Heap before:");
    debug_heap(stdout, HEAP_START);
    void* mem1 = _malloc(REGION_MIN_SIZE * 2);
    puts("Test 6. Heap after:");
    debug_heap(stdout, HEAP_START);
    struct block_header* mem1_header = (struct block_header*) (((uint8_t*)mem1)-offsetof(struct block_header, contents));;
    assert(mem1_header != mmap_addr);
    _free(mem1);
    heap_term();
}

int main() {
    test_successful_allocation();
    test_free_one();
    test_free_two();
    test_same_region_extension();
    test_different_region_extension();
    return 0;
}
